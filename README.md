# rattrapage2019

1_ Java	distingue	deux	types	de	flux	:	les	flux	de	caractères	et	les	flux	binaires.
2- FileReader - LineNumber - ReaderInputStream - DataInputStream
	
3- DataInputStream utilise la methode Reader

4_ La précaution qu'on doit systémtiquement prendre c'est d'ouvrir un fichier dans un bloc try catch.
5_ la classe fournie par l'API Java I/O qui permet de lire les fichiers GZIP : GZIPInputStream.
6_ a. Pour construire une instance de GZIPInputStream, il faut lui passer en paramètre à son constructeur une instance de FileInputStream.
b. Le pattern utilisé ici est "Décoration".
8- 	LineNumberReader
 	LineNumberReader line =  new LineNumberReader(fichier) ;
9_ le nombre de ligne est 4404.

7_ a. La classe qui permet de lire un fichier texte à partir d'un flux binaire est DataInputStream.
b. Il faut lui passer en paramètre de son constructeur une instance de FileInputStream.
10- La méthode boolean isEmpty() 

11- La méthode trim()
12_ le nombre de ligne non vide est 4101.

Exxercice 2
1- Les interfaces Collection et Map.
2 les quatres interfaces sont collection list set et sortedset
3_ l'interface est set.
5_ les trois premieres titres :
	The Wolf and the Lamb 
	The Bat and the Weasels 
	The Ass and the Grasshopper
Exercice 3:
__Q1__:
Un bean est une classe Java qui doit posséder les propriétés suivantes :

- elle doit implémenter Serializable ;
- elle doit posséder un constructeur vide, soit celui qui existe par défaut, soit déclaré explicitement ;
- elle doit exposer des propriétés, sous forme de paires getters / setters .